In the previous steps we became familiar with creating and running unittests. However, it would be good to have insights in how much of our code is actually covered by our tests. Specifically, if we want to improve our test coverage, it would be good to know which parts of our code is not touched by the tests.

In order to do so, we install two extra modules `pytest` and `pytest-cov`. These can be installed with pip:
`pip install -r ~/tictactoe/requirements.txt`{{ execute }}

After installing these dependencies, we can run the tests again and create a report of the coverage of our tests. This can be done with:
`coverage run -m pytest --cov tictactoe --cov-report term-missing`{{ execute }}

Note that this command will create a file `.coverage` which can be used for more analysis with the coverage tool as well; `coverage help`{{ execute }} is your friend ;-)